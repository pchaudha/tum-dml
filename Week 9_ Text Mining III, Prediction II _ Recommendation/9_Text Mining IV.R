
setwd("E:/Projekte/scrapyMashable/Mashable Data Set")

#Setup Environment
rm(list = ls())                                 # Clear Environment
cat("\014")                                     # Clear Console
set.seed(42)                                    # Set the seed to 42 

data  = read.csv("E:/Projekte/scrapyMashable//Mashable Data Set/DataSet_Version_0.07.csv",sep = ",")
#######################################################################################
# In case Java is not Running
if (Sys.getenv("JAVA_HOME") != "")
  Sys.setenv(JAVA_HOME = "")

# Packages
if (!require(tm))           install.packages("tm")
if (!require(slam))         install.packages("slam")
if (!require(FSelector))    install.packages('FSelector')
if (!require(FSelector))    install.packages('plyr')
if (!require(FSelector))    install.packages('SnowballC')

library(tm)
library(slam)
library(FSelector)
library(plyr)
library(SnowballC)
#######################################################################################
# Data Preparation
data = within(data , rm(url, year_url, month_url, day_url, title_html, author_html, 
                        datetime_html, shares_facebook_html, shares_twitter_html, 
                        shares_google_plus_html,shares_linked_in_html,
                        shares_stumble_upon_html, shares_pinterest_html,
                        channel_html, content_source_type_html,
                        content_source_name_html, age_html, sourced_from_html,
                        campaign_html, viral_video_type_html, h_pub_html,
                        top_channel_html, canonical_url_html, num_hrefs_tag,
                        num_images_tag, num_paragraphs_tag, num_iframes_tag,
                        num_figures_tag, num_keywords, meta_title_length,
                        meta_description_length, num_words, meta_twitter_title_length))
  
data[data             == '?'] <- NA             # Replace missing values with "NA"
colSums(is.na(data))                            # Show colums with missing values
data = data[complete.cases(data),]              # Only instances without NAs
colSums(is.na(data))                            # Show colums with missing values
#######################################################################################
# Feature Creation: Dependend Variable: Exact number of shares
data$shares <- NA
data$shares <- floor(data$shares_html/100)*100  # Bin the shares to groups of hundred

# Feature Creation: Dependend Variable: Popular - Yes(1)/No(0)?
data$shares_binary <- NA 

for (i in 1:nrow(data)) {
  shares <- data$shares[i]
  if (shares >= 1400) {
    data$shares_binary[i] <- 1                 # Popular
  } else {
    data$shares_binary[i] <- 0                 # Unpopular
  }
}
rm(shares)
data$shares_html <- NULL
#######################################################################################
## Text Mining on article_title_html
# 1) Keep only terms that occur more than 10 times in the titles
corpus_article_title_html <- VCorpus(VectorSource(data$article_title_html))                  # Construct corpus
corpus_article_title_html <- tm_map(corpus_article_title_html, content_transformer(tolower)) # Convert to lower case
corpus_article_title_html <- tm_map(corpus_article_title_html, stripWhitespace)              # Eliminate Extra Whitespaces
corpus_article_title_html <- tm_map(corpus_article_title_html, PlainTextDocument)            # Preprocessed documents be treated as text documents

# dataframe <- data.frame(article_title_html = unlist(sapply(corpus_article_title_html, `[`, "content")), stringsAsFactors = F) # Transform Corpus into dataframe
# data$article_title_html <- dataframe$article_title_html               # Update author with data prepared version of authors
# rm(dataframe)                                                         # Remove dataframe object

dtm_article_title_html    <- DocumentTermMatrix(corpus_article_title_html)                  # Create Document Term Matrix dtm for corpus_article_title_html
dim(dtm_article_title_html)                                           # Inspect the dtm
colTotals <- col_sums(dtm_article_title_html)                         # Find the total occurances of each word in all docs colTotals
dtm_article_title_html    <- dtm_article_title_html[,which(colTotals > 10)]                # Keep only  words that occur >100 times in all docs
dim(dtm_article_title_html)                                           # Inspect the dtm

rm(colTotals)
rm(corpus_article_title_html)

# # 2) Keep only term that has a gain ratio
dtm_article_title_html <- as.matrix(dtm_article_title_html)           # Transform dtm into a matrix
dtm_article_title_html <- as.data.frame(dtm_article_title_html)       # Transform dtm into a dataframe    
dtm_article_title_html$shares_binary <- data$shares_binary            # Copy shares_binary attribute to dtm_article_title_html

# gain_ratio = gain.ratio(shares_binary  ~ ., data = dtm_article_title_html) # Calculate gain ratios for all terms
# gain_ratio = na.omit(gain_ratio)                                      # Remove all instances with NA Values as gain ratio
# 
# keep = row.names(gain_ratio)                                        # Create a chr with columns to keep
# dtm_article_title_html <- dtm_article_title_html[keep]              # Remove all columns that are not in that chr
# dtm_article_title_html$shares_binary <- data$shares_binary          # Copy shares_binary attribute to dtm_article_title_html
# dim(dtm_article_title_html)                                         # Inspect the dtm

# 3) Calculate probabilities: If the term occurs how probably is this article popular or unpopular?
temp <- dtm_article_title_html
temp$shares_binary <- NULL
result <- data.frame(matrix(NA, ncol = 3, nrow = length(temp)))       # Construct a data frame for saving the results
result$X1 <- names(temp)
names(result) <- c("term","count_appear_y","count_appear_n")
rm(temp)

result  = read.csv("E:/Projekte/scrapyMashable//Mashable Data Set/prob_1_4045.csv",sep = ",")

# result <- gain_ratio                                                  # Copy the gain_ratio data frame as result data frame
# result$attr_importance <- NULL                                        # Remove the gain ratios
# result$prob_count_appear_y_count_appear <- NA                         # Probability I
# result$prob_count_appear_n_count_appear <- NA                         # Probability II
# result$prob_count_not_appear_y_count_not_appear <- NA                 # Probability III
# result$prob_count_not_appear_n_count_not_appear <- NA                 # Probability IV

index <- length(dtm_article_title_html) - 1                           # Count the number of attributes exept shares_binary
index <- 4045
for (i in 4000:index) {
  count_appear <- 0                                                   # Variable counts how often the term apprears in all documents
  count_not_appear <- 0                                               # Variable counts how often the term does not apprears in all documents
  count_appear_y <- 0                                                 # Variable counts how often shares_binary = 1 if the term appears
  count_appear_n <- 0                                                 # Variable counts how often shares_binary = 0 if the term appears
  # count_not_appear_y <- 0                                             # Variable counts how often shares_binary = 1 if the term does not appears
  # count_not_appear_n <- 0                                             # Variable counts how often shares_binary = o if the term does not appears
  
  number_row <- nrow(dtm_article_title_html)                          # Count the total number of rows
  save <- count(dtm_article_title_html[,i])                           # Count the frequencies of the appearance of the term and save it as data frame
  count_not_appear <- save[1,2]                                       # How often is the term not accuring?
  count_appear <- number_row - count_not_appear                       # How often is the term accuring?

  
  for (j in 1:nrow(dtm_article_title_html)) {
    if ((dtm_article_title_html[j,i] >= 1) && (dtm_article_title_html$shares_binary[j] == 1) )
      count_appear_y <- (count_appear_y + 1)
    if ((dtm_article_title_html[j,i] >= 1) && (dtm_article_title_html$shares_binary[j] == 0) )
      count_appear_n <- (count_appear_n + 1)
    # if ((dtm_article_title_html[j,i] == 0) && (dtm_article_title_html$shares_binary[j] == 1) )
    #   count_not_appear_y <- (count_not_appear_y + 1)
    # if ((dtm_article_title_html[j,i] == 0) && (dtm_article_title_html$shares_binary[j] == 0) )
    #   count_not_appear_n <- (count_not_appear_n + 1)
  }
  
  result[i,2] = round(count_appear_y/count_appear, 2)
  result[i,3] = round(count_appear_n/count_appear, 2)
  # result[i,3] = round(count_not_appear_y/count_not_appear, 2)
  # result[i,4] = round(count_not_appear_n/count_not_appear, 2)

  status = c("Done: Column", i)
  print(status)
}

write.csv(result, file = "prob_1_4045.csv", row.names = FALSE)
#######################################################################################
# 4) Evaluate result data frame and construct combined features
result  = read.csv("E:/Projekte/scrapyMashable//Mashable Data Set/prob_1_4045.csv",sep = ",")
result <- na.omit(result)

result$term_in_title_high_prob_popular[result$count_appear_y >= 0.80] <- 1
result$term_in_title_medium_prob_popular <- NA

for (i in row(result))
  if ((result$count_appear_y[i] < 0.80) && (result$count_appear_y[i] >= 0.60))
    result$term_in_title_medium_prob_popular[i] = 1

result$term_in_title_high_prob_unpopular[result$count_appear_n >= 0.80] <- 1
result$term_in_title_medium_prob_unpopular <- NA

for (i in row(result))
  if ((result$count_appear_n[i] < 0.80) && (result$count_appear_n[i] >= 0.60))
    result$term_in_title_medium_prob_unpopular[i] = 1

result$count_appear_y <- NULL
result$count_appear_n <- NULL

# result$count_appear_y >= 0.80
r_high_prob_popular <- result
r_high_prob_popular$term_in_title_medium_prob_popular <- NULL
r_high_prob_popular$term_in_title_high_prob_unpopular <- NULL
r_high_prob_popular$term_in_title_medium_prob_unpopular <- NULL
r_high_prob_popular <- na.omit(r_high_prob_popular)
r_high_prob_popular_keep <- as.character(r_high_prob_popular$term)

dtm_high_prob_popular <- dtm_article_title_html[r_high_prob_popular_keep]
dtm_high_prob_popular$row_sum <- rowSums(dtm_high_prob_popular)

dtm_high_prob_popular$term_in_title_high_prob_popular <- 0
dtm_high_prob_popular$term_in_title_high_prob_popular[dtm_high_prob_popular$row_sum >= 1] <- 1
data$term_in_title_high_prob_popular <- dtm_high_prob_popular$term_in_title_high_prob_popular

# result$count_appear_y[i] < 0.80) && (result$count_appear_y[i] >= 0.60
r_medium_prob_popular <- result
r_medium_prob_popular$term_in_title_high_prob_popular <- NULL
r_medium_prob_popular$term_in_title_high_prob_unpopular <- NULL
r_medium_prob_popular$term_in_title_medium_prob_unpopular <- NULL
r_medium_prob_popular <- na.omit(r_medium_prob_popular)
r_medium_prob_popular_keep <- as.character(r_medium_prob_popular$term)

dtm_medium_prob_popular <- dtm_article_title_html[r_medium_prob_popular_keep]
dtm_medium_prob_popular$row_sum <- rowSums(dtm_medium_prob_popular)

dtm_medium_prob_popular$term_in_title_medium_prob_popular <- 0
dtm_medium_prob_popular$term_in_title_medium_prob_popular[dtm_medium_prob_popular$row_sum >= 1] <- 1
data$term_in_title_medium_prob_popular <- dtm_medium_prob_popular$term_in_title_medium_prob_popular


# result$count_appear_n >= 0.80
r_high_prob_unpopular <- result
r_high_prob_unpopular$term_in_title_high_prob_popular <- NULL
r_high_prob_unpopular$term_in_title_medium_prob_popular <- NULL
r_high_prob_unpopular$term_in_title_medium_prob_unpopular <- NULL
r_high_prob_unpopular <- na.omit(r_high_prob_unpopular)
r_high_prob_unpopular_keep <- as.character(r_high_prob_unpopular$term)

dtm_high_prob_unpopular <- dtm_article_title_html[r_high_prob_unpopular_keep]
dtm_high_prob_unpopular$row_sum <- rowSums(dtm_high_prob_unpopular)

dtm_high_prob_unpopular$term_in_title_high_unprob_popular <- 0
dtm_high_prob_unpopular$term_in_title_high_unprob_popular[dtm_high_prob_unpopular$row_sum >= 1] <- 1
data$term_in_title_high_prob_unpopular <- dtm_high_prob_unpopular$term_in_title_high_unprob_popular


# result$count_appear_n[i] < 0.80) && (result$count_appear_n[i] >= 0.60
r_medium_prob_unpopular <- result
r_medium_prob_unpopular$term_in_title_high_prob_popular <- NULL
r_medium_prob_unpopular$term_in_title_medium_prob_popular <- NULL
r_medium_prob_unpopular$term_in_title_high_prob_unpopular <- NULL
r_medium_prob_unpopular <- na.omit(r_medium_prob_unpopular)
r_medium_prob_unpopular_keep <- as.character(r_medium_prob_unpopular$term)

dtm_medium_prob_unpopular <- dtm_article_title_html[r_medium_prob_unpopular_keep]
dtm_medium_prob_unpopular$row_sum <- rowSums(dtm_medium_prob_unpopular)

dtm_medium_prob_unpopular$term_in_title_medium_prob_unpopular <- 0
dtm_medium_prob_unpopular$term_in_title_medium_prob_unpopular[dtm_medium_prob_unpopular$row_sum >= 1] <- 1
data$term_in_title_medium_prob_unpopular <- dtm_medium_prob_unpopular$term_in_title_medium_prob_unpopular
#######################################################################################
data$meta_title_html <- NULL
data$meta_description_html <- NULL
data$meta_keywords_html <- NULL
data$meta_twitter_title_html <- NULL
data$article_title_html <- NULL
data$shares <- NULL
data$shares_binary <- NULL


write.csv(data, file="Probability_Features_Niklas.csv", row.names=FALSE)
#######################################################################################
#######################################################################################
#######################################################################################
# Find Patterns Combinations
dtm_article_title_html$row_sum <- NA
index <- length(dtm_article_title_html) - 2
dtm_article_title_html$row_sum <- rowSums(dtm_article_title_html[0:index]) # Count how many similar terms with high accuracy are one title
count(dtm_article_title_html$row_sum)

pattern_2 <- dtm_article_title_html[(dtm_article_title_html$row_sum == 2),]
pattern_2$hasPattern <- NULL
pattern_2$row_sum <- NULL
pattern_2$shares_binary <- NULL

pattern_2$pattern <- apply(pattern_2, 1, FUN = detection)

count(pattern_2$pattern)

detection <- function(x) {
  paste(names(pattern_2)[which(x == 1)], collapse = ",")
}


pattern_3 <- dtm_article_title_html[(dtm_article_title_html$row_sum == 3),]
pattern_3$hasPattern <- NULL
pattern_3$row_sum <- NULL
pattern_3$shares_binary <- NULL

pattern_3$pattern <- apply(pattern_3, 1, FUN = detection)

count(pattern_3$pattern)

detection <- function(x) {
  paste(names(pattern_3)[which(x == 1)], collapse = ",")
}

dtm_article_title_html$hasPattern[dtm_article_title_html$row_sum >= 1] <- "y" 
dtm_article_title_html$hasPattern[dtm_article_title_html$row_sum == 0] <- "n" 
gain_ratio2 = gain.ratio(shares_binary  ~ ., data = dtm_article_title_html) # Calculate gain ratios for all terms

dtm_article_title_html

most_important_attributes <- cutoff.k(gain_ratio, "NaN") #Select the most important attributes
most_important_attributes

# inspect(dtm_article_title_html[1:10, 0:2])
#######################################################################################
#######################################################################################
#######################################################################################