setwd("C:/Books/MS Informatics/2nd Semester/Data Mining Practical/News articles popularity/OnlineNewsPopularity")

#install.packages('dplyr')
library(dplyr)

# File that containes the articles' text content
article_content = read.csv("DataSets\\article_text_content.csv")

# The main data set
dataset = read.csv("DataSets\\Dataset_Version_0.6.csv")

# Sort the datasets by the title to merge them later on
article_content = arrange(article_content, title)
dataset = arrange(dataset, title_url)

# Merging the channel field to the article content dataset
merged = cbind(dataset$channel_html, article_content)

# TODO: Split 'merged' by newly added channel field and analyse articles per channel

#Rename column
colnames(merged)[1] = "channels" 

#New files for each channel
tech <- subset(merged, channels %in% c("tech"))
write.csv(tech, file="tech.csv", row.names=FALSE)

world <- subset(merged, channels %in% c("world"))
write.csv(world, file="world.csv", row.names=FALSE)

entertainment <- subset(merged, channels %in% c("entertainment"))
write.csv(entertainment, file="entertainment.csv", row.names=FALSE)

business <- subset(merged, channels %in% c("business"))
write.csv(business, file="business.csv", row.names=FALSE)

watercooler <- subset(merged, channels %in% c("watercooler"))
write.csv(watercooler, file="watercooler.csv", row.names=FALSE)

socialmedia <- subset(merged, channels %in% c("social-media"))
write.csv(socialmedia, file="socialmedia.csv", row.names=FALSE)

lifestyle <- subset(merged, channels %in% c("lifestyle"))
write.csv(lifestyle, file="lifestyle.csv", row.names=FALSE)

