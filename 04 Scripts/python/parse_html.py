#!/usr/bin/python3

#
# !!! Quick and dirty implementation due to time pressure in week 3 :( !!!
# This script reads in all html files in the specified directory
# and extracts a bunch of meta data from each html file using
# xpath queries. Afterwards it write the extracted features to a csv-file
# with the title as key in the first column for merging purposes.
#

from lxml import html
from os import listdir, stat
from os.path import isfile, join
from json import loads
from sys import exit

separator = ';'

# Directory where the html files are located
root_dir = '/home/spectre/data_mining_lab/OnlineNewsPopularity/html_files/'

html_files = [f for f in listdir(root_dir) if isfile(join(root_dir, f))]

header_strings = ['author_html', 'datetime_html','shares_html', 'shares_facebook_html', 'shares_twitter_html', 'shares_google_plus_html', 'shares_linked_in_html', 'shares_stumble_upon_html', 'shares_pinterest_html', 'canonical_url_html', 'meta_title_html', 'meta_description_html', 'meta_keywords_html', 'meta_twitter_title_html']
xpath_strings = ['//meta[@name="author"]/@content', 
    '//meta[@name="date"]/@content', 
    '//div[@class="total-shares"]/em/child::text()', 
    '//a[@class="social-stub social-share facebook"]/@data-shares',
    '//a[@class="social-stub social-share twitter"]/@data-shares',
    '//a[@class="social-stub social-share google_plus"]/@data-shares',
    '//a[@class="social-stub social-share linked_in"]/@data-shares',
    '//a[@class="social-stub social-share stumble_upon"]/@data-shares',
    '//a[@class="social-stub social-share pinterest"]/@data-shares',
    '//link[@rel="canonical"]/@href',
    '//meta[@property="og:title"]/@content',
    '//meta[@property="og:description"]/@content',
    '//meta[@name="keywords"]/@content',
    '//meta[@name="twitter:title"]/@content']

feature_data = {}
json_keys = ['channel', 'content_source_type', 'content_source_name', 'age', 'sourced_from', 'campaign', 'viral_video_type', 'h_pub', 'top_channel']

#header_strings = ['id']
#xpath_strings = ['//section[@class="article-content"]//a']

# Iterate all files in the given root_dir
for el in html_files:
    with open(root_dir+el, 'r') as html_doc:
        
        feature_id = el.split('_')[0]   
        print(feature_id)
        
        title = el.split('_')[1].split('.')[0]

        feature_id = title        

        if stat(root_dir+el).st_size == 0:
            for i in range(0, len(xpath_strings)+len(json_keys)-1):
                if i == 0:
                    feature_data[feature_id] = '"NA"'
                else:
                    feature_data[feature_id] += separator + '"NA"'
            continue
        
        file_content = html_doc.read()

        
        feature_data[feature_id] = ''

        buf = file_content.split('<script>')[1].split('</script')[0]
        print(buf)
        buf = buf.split('=')[1].split('window._gaq')[0].rstrip().lstrip()
        buf = buf[:-1]
        print(buf)
        json_dict = loads(buf)
        #print(json_dict)
        
        for json_key in json_keys:
            feature_data[feature_id] += '"NA"'+separator if json_dict[json_key] is None else '"'+str(json_dict[json_key]).replace(';', ',').replace('"', "'")+'"'+separator
        
        #print(feature_data)
        #exit()

        tree = html.fromstring(file_content)
        for i in range(0, len(xpath_strings)):
            feature_value = tree.xpath(xpath_strings[i])
            #print(feature_value)
            
            if not feature_value:
                if i == len(xpath_strings)-1:
                    feature_data[feature_id] += '"NA"'
                else:
                    feature_data[feature_id] += '"NA"' + separator
            else:
                try:
                    lookup = {'k':1000, 'K': 1000, 'm': 1000000, 'M': 1000000}
                    unit = feature_value[0][-1]
                    if unit in lookup:
                        feature_value[0] = int(float(feature_value[0][:-1]) * lookup[unit])
                except ValueError:
                    print('Not a number')
                print(feature_value[0])
   
                if i == len(xpath_strings)-1:
                    feature_data[feature_id] += '"'+str(feature_value[0]).replace('\n', '').replace(';', ',').replace('"', "'")+'"'
                else:
                    feature_data[feature_id] += '"'+str(feature_value[0]).replace('\n', '').replace(';', ',').replace('"', "'")+ '"' + separator
#print(feature_data)

# Write extracted data to file
with open('./new_features_from_html.csv', 'w') as csv_file:
    csv_file.write('title_html'+separator)
    for idx, val in enumerate(json_keys):
        val += '_html'
        csv_file.write(''+val if idx==0 else separator+val)
    csv_file.write(separator)
    for idx, val in enumerate(header_strings):
        csv_file.write(''+val if idx==0 else separator+val)
    csv_file.write('\n')
    
    for key in feature_data.keys():
        csv_file.write(key+separator+feature_data[key]+'\n')

