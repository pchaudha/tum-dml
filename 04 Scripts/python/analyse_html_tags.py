#!/usr/bin/python3

from lxml import html, etree
import pickle

test = 'robot-chicken-animate'

articles_text_data = {}

feature_data = {}
link_tag = 'a'
img_tag = 'img'
paragraph_tag = 'p'
#video_tag = 'video'
iframe_tag = 'iframe'
#twitter_widget_tag = 'div[@data-scribe="page:tweet"]'
#viral_video_lead_tag = 'div[@class="viral-video-lead"]'
figure_tag = 'figure'

# Load the raw html data again as dictionary
with open('article_html_data.bin', 'rb') as f:
    data = pickle.load(f)

print(len(data))

for key in data:
    #print('Processing article with title "{}"'.format(key))
    tree = html.fromstring(data[key])
    feature_data[key] = {}
    feature_data[key][link_tag] = int(tree.xpath('count(//{})'.format(link_tag)))
    feature_data[key][img_tag] = int(tree.xpath('count(//{})'.format(img_tag)))
    feature_data[key][paragraph_tag] = int(tree.xpath('count(//{})'.format(paragraph_tag)))
    #feature_data[key][video_tag] = int(tree.xpath('count(//{})'.format(video_tag)))
    feature_data[key][iframe_tag] = int(tree.xpath('count(//{})'.format(iframe_tag)))   
    #feature_data[key][twitter_widget_tag] = int(tree.xpath('count(//{})'.format(twitter_widget_tag)))
    #feature_data[key][viral_video_lead_tag] = int(tree.xpath('count(//{})'.format(viral_video_lead_tag))) 
    feature_data[key][figure_tag] = int(tree.xpath('count(//{})'.format(figure_tag)))
    
    #if feature_data[key][video_tag] > 0:
    #    print('Found video: {}'.format(feature_data[key][video_tag]))
    #for element in tree.iter():
    #    print("%s - %s" % (element.tag, element.text))

    #print(feature_data)
    #break

print(len(feature_data))

with open('tag_features.csv', 'w') as f:
    f.write('{},{},{},{},{},{}\n'.format('title',link_tag, img_tag, paragraph_tag, iframe_tag, figure_tag))
    for key in feature_data:
        f.write('"{}",'.format(key))
        for idx, inner_key in enumerate(feature_data[key].keys()):
            f.write('{}\n'.format(feature_data[key][inner_key]) if idx == len(feature_data[key])-1 else '{},'.format(feature_data[key][inner_key]))
