#!/usr/bin/python3

#
# This scripts reads in all html files given in the specified directory
# and parsed the article content section. Afterwards it saves the 
# data to a file and additionally saves the article text data
# (html tags removed) to a different file.
#

from lxml import html, etree
from os import listdir, stat
from os.path import isfile, join
from sys import exit
import pickle

separator = ';'

# Directory where the html files are located
root_dir = '/home/spectre/data_mining_lab/OnlineNewsPopularity/html_files/'

html_files = [f for f in listdir(root_dir) if isfile(join(root_dir, f))]

data = {}

nas = 0

# Read in every html file and process it
for el in html_files:
    with open(root_dir+el, 'r') as html_doc:
            
        title = el.split('_')[1].split('.')[0]
        #print(title)
        file_content = html_doc.read()
        
        # Some known error to fix
        if title == 'twitter-bot-tofu':
            title += '_product'
    
        data[title] = ''

        # The file is empty
        if stat(root_dir+el).st_size == 0:
            data[title] = '"NA"'
            continue

        tree = html.fromstring(file_content)
        node = tree.xpath('//section[@class="article-content"]')
        # The xpath query gives no result
        if not node:
            print('Article content section not found: '+ title)
            nas += 1
            data[title] = '"NA"'
            continue
        
        article_raw_content = etree.tostring(node[0], pretty_print=True)
        #print(article_raw_content)
        data[title] = article_raw_content
     

# Write the article 
with open('article_html_data.bin', 'wb') as f:
    pickle.dump(data, f, pickle.HIGHEST_PROTOCOL)


"""
# Load the raw html data again as dictionary
with open('article_html_data.bin', 'rb') as f:
    data = pickle.load(f)
"""

for key in data:
    print('Processing article with title "{}"'.format(key))
    tree = html.fromstring(data[key])
     
    # Remove every script tag te prevent some javascript code in the text content
    for script_tag in tree.xpath("//script"):
      script_tag.getparent().remove(script_tag)

    # Get the text of the html without tags and remove whitespaces and newlines
    data[key] = tree.text_content().strip().replace('\n', '')


# Store the article text data as dictionary
with open('article_text_data.bin', 'wb') as f:
    pickle.dump(data, f)

print(nas)  
