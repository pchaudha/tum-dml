#######################################################################################
### Author: Niklas Eschner
### Data Mining Lab - Mashable Data Set
### Prediction I
#######################################################################################
### 1) Imports and Declarations

##  1.1) Import Libraries

# In case Java is not running
if (Sys.getenv("JAVA_HOME") != "")
  Sys.setenv(JAVA_HOME = "")

if (!require(caret))        install.packages('caret')
if (!require(FSelector))    install.packages('FSelector')
if (!require(randomForest)) install.packages('RWeka')
if (!require(randomForest)) install.packages('randomForest')

library(caret)
library(FSelector)
library(RWeka)
library(randomForest)

##  1.2) Declarations
setwd("E:/Projekte/scrapyMashable/Mashable Data Set") # Niklas' Workspace:

rm(list = ls())                                 # Clear Environment
cat("\014")                                     # Clear Console
set.seed(42)                                    # Set the seed to 42 

data  = read.csv("E:/Projekte/scrapyMashable//Mashable Data Set/Dataset_Version_0.1_10percent.csv", sep = ",")
#######################################################################################
### 2) Data Preparation
data$shares_binary <- as.factor(data$shares_three_classes_1)
data$shares_three_classes_1 <- as.factor(data$shares_three_classes_1)
data$shares_three_classes_2 <- as.factor(data$shares_three_classes_2)
data$year <- as.factor(data$year)
data$author_single <- as.factor(data$author_single)
data$author_frequent <- as.factor(data$author_frequent)
data$author_regular <- as.factor(data$author_regular)
data$author_valuable <- as.factor(data$author_valuable)
data$content_third_party <- as.factor(data$content_third_party)
data$campaign_facebook  <- as.factor(data$campaign_facebook)
data$video_viral <- as.factor(data$video_viral)

data$id <- NULL
data$title_url <- NULL
data$shares_html <- NULL
data$shares <- NULL
data$shares_three_classes_1 <- NULL
data$shares_three_classes_2 <- NULL
data$published_articles <- NULL
data$author_sum_totalShares <- NULL

str(data)                                       # Show structure of data
data[data             == '?'] <- NA             # Replace missing values with "NA"
colSums(is.na(data))                            # Show colums with missing values
data = data[!is.na(data$channel), ]             # Remove instances with missing values
data = data[!is.na(data$author_frequent), ]     # Remove instances with missing values
colSums(is.na(data))                            # Show colums with missing values
#######################################################################################
### 3) Feature Selection
##  3.1) Calculate weights for the attributes using Info Gain and Gain Ratio
weights_info_gain = information.gain(shares_binary  ~ ., data = data)
weights_gain_ratio = gain.ratio(shares_binary  ~ ., data = data)
weights_gain_ratio                              # Display weights_gain ratio

## Select the most important attributes based on Gain Ratio
most_important_attributes <- cutoff.k(weights_gain_ratio, 11)
drop <- c("author")                             # Drop attributes
most_important_attributes <- most_important_attributes [!most_important_attributes %in% drop]
most_important_attributes

formula_with_most_important_attributes <- as.simple.formula(most_important_attributes, "shares_binary")
formula_with_most_important_attributes
#######################################################################################
### 4) Training
##  4.1) Splitting Data Set in Training an Test Data
fitCtrl = trainControl(method = "LGOCV", p = .80, number = 1)
# fitCtrl = trainControl(method = "repeatedcv", number = 5, repeats = 2, savePredictions = TRUE)

##  4.3) Training models
model_one = train(formula_with_most_important_attributes, data = data, method = "OneR", trControl = fitCtrl, metric = "Accuracy")
model_two = train(formula_with_most_important_attributes, data = data, method = "J48", trControl = fitCtrl, metric = "Accuracy")

##  4.4) Evaluation
model_one
model_two
evaluation = resamples(list(oneRule = model_one, treeJ48 = model_two))
summary(evaluation)

confMatx <- list(tree = confusionMatrix(model_two), oneRule = confusionMatrix(model_one))
confMatx