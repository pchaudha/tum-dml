import scrapy

from scrapyMashable.items import ScrapymashableItem

class MashableSpider(scrapy.Spider):
    name = "mashable"
    allowed_domains = ["mashable.com"]
    start_urls = [
    # Insert 39,643 URLs here
    ]

    def parse(self, response):
        global id
        item = ScrapymashableItem()
        item['url'] = response.xpath('//link[contains(@rel, "canonical")]/@href').extract()
        item['content'] = response.xpath('//section[contains(@class, "article-content")]//text()').extract()
        yield item

