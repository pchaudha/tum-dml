import scrapy
import os.path

id = 1

from scrapyMashable.items import ScrapymashableItem

class MashableSpider(scrapy.Spider):
    name = "mashable"
    allowed_domains = ["mashable.com"]
    start_urls = [
    # Insert 39,643 URLs here
    ]

    def parse(self, response):
        global id
        save_path = 'E:\Projekte\scrapyMashable\HTML Download'
        filename = response.url.split("/")[-2]
        editfilename = str(id)+"_"+filename+".html"
        id += 1
        completeName = os.path.join(save_path, editfilename)
        open(completeName, 'wb').write(response.body)

